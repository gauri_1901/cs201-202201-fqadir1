# Lab 0

## Total

14/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       4/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          0/2
  * Correct output matches pseudocode   0/2
* Documentation                         0/2

## Comments
