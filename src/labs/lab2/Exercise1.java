package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		
	Scanner in = new Scanner (System.in);
	
	System.out.println("Enter number for square: "); //reading value for int n
	
	int n=in.nextInt();
	
	System.out.println("Enter *:"); // assigning asterisk
	
	char c = in.next().charAt(0); //character to print
	
	for(int i=0;i<n;i++) { //loop 1 iterates for i<n
		
		for(int j=0;j<n;j++) { //loop 2 iterates for j<n
			System.out.print(c);
		}
	
		System.out.println(); 
	}
	in.close();
	
	}
	}
	
