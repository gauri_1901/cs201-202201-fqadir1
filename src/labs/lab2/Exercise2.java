package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
	
		int number = 0;
		float sum = 0;
		float grade = 0;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("How many grades?:");
		number = input.nextInt();  //asks for n value
		
		for (int i = 0; i< number; i++) { //asks to repeat number (n) 
			
			System.out.println("Enter grade" + (i + 1) + " : "); //asks for grades and add to the provided number as input in : n
			grade = input.nextFloat(); 
			
					sum += grade;
					
		}
		
		
		System.out.println("Average = " + sum/(float)number); //arithmetic
		
	
		
		input.close();
		
	}

}
