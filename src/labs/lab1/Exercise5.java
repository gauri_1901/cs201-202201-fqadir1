package labs.lab1;

import java.util.Scanner;

public class Exercise5 {

	public static void main(String[] args) {
		
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter the length ");
		double L = in.nextDouble();
				
		System.out.println("Enter the width ");
		double W = in.nextDouble();
		
		System.out.println("Enter the depth ");
		double D = in.nextDouble();
		
		double sqft = L * W * D;
		
		System.out.println("Square Footage of wood needed:" + sqft + " sq^2");
		
	}

}
