package labs.lab1;

import java.util.Scanner;

public class Exercise6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter the inches ");
		double inches = in.nextDouble();
		
		double centimeters = inches * 2.54;
		
		System.out.println(inches + " inches is " + centimeters + " centimeters");
	}

}
